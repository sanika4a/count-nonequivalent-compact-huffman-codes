
INCLUDE=-I/usr/include/flint
LIB=

SHELL=bash

.PHONY: all timings

all: number_unit_fractions

timings: timings-c_t2.txt timings-c_N32768.txt

number_unit_fractions.o: number_unit_fractions.c
	gcc -c $(INCLUDE) $<

number_unit_fractions: number_unit_fractions.o
	gcc $< -o$@  $(LIB) -l flint #-l ntl -l mpir -l gmp -l gf2x

timings-c_t2.txt: number_unit_fractions
	rm -f $@
	for N in {8..15}; do ./number_unit_fractions 2 $$((2**$$N)) | tee -a $@; done

timings-%-table.tex: timings-%.txt
	cat $< \
	| sed 's|[^=]*=||' \
	| sed 's|,[^=:]*[=:][ ]*| \& |g' \
	| sed 's|$$|\\\\|' \
	> $@

timings-c_N32768.txt: number_unit_fractions
	rm -f $@
	for t in {2..30}; do ./number_unit_fractions $$t 32768 | tee -a $@; done

test: number_unit_fractions
	./number_unit_fractions 2 256 && \
	./number_unit_fractions 9 256 && \
	diff -u output/result_t=2_N=256.txt result_t=2_N=256.saved && \
	diff -u output/result_t=9_N=256.txt result_t=9_N=256.saved

clean:
	rm -vf number_unit_fractions.o number_unit_fractions
	rm -vf *~
