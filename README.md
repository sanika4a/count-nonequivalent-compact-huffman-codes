# Algorithmic counting of nonequivalent compact Huffman codes

This repository contains code accompanying the article
[**Algorithmic counting of nonequivalent compact Huffman codes**](https://arxiv.org/abs/1901.11343)
by [Christian Elsholz](https://www.math.tugraz.at/~elsholtz),
[Clemens Heuberger](http://wwwu.uni-klu.ac.at/cheuberg/)
and [Daniel Krenn](http://www.danielkrenn.at/).

# Install / Run the program (Variant "Docker")

## Install Docker

Follow the instructions for installing
[Docker CE](https://docs.docker.com/install) and
[Docker Compose](https://docs.docker.com/compose/install).

## Build Docker image
```
docker-compose build
```

## Test

Test via
```
docker-compose run -e USER_ID=$UID unit-fractions make test
```

## Compute

Compute values via
```
docker-compose run -e USER_ID=$UID unit-fractions number_unit_fractions 2 128
```
the output will appear in `output/`.

# Install / Run the program (Variant "direct")

Make sure that [FLINT: Fast Library for Number Theory](http://www.flintlib.org)
is installed on your system and compile and run
[number_unit_fractions.c](./number_unit_fractions.c).
The [Makefile](./Makefile) gives you hints on how exactly.